#!/bin/bash

rm -rf ~/schema-temp
mkdir ~/schema-temp
cd ~/schema-temp
mkdir src
cd src
mkdir bitbucket.org
cd bitbucket.org
mkdir boguswojcik
cd boguswojcik

export GOPATH=~/schema-temp

printf "TESTING: Testing mood-producer...\n"
cd ~/schema-temp/src/bitbucket.org/boguswojcik
git clone -q git@bitbucket.org:boguswojcik/mood-producer.git
cd ./mood-producer
dep ensure --vendor-only
cp ~/go/src/bitbucket.org/boguswojcik/mood-schema/mood.proto ./vendor/bitbucket.org/boguswojcik/mood-schema/
go generate ./
if go build main.go ; then
    printf "SUCCESS: Integration test for mood-producer succeeded.\n\n"
else
    printf "FAILURE: Integration test for mood-producer failed.\n\n"
fi

printf "TESTING: Testing happy-consumer...\n"
cd ~/schema-temp/src/bitbucket.org/boguswojcik
git clone -q git@bitbucket.org:boguswojcik/happy-consumer.git
cd ./happy-consumer
dep ensure --vendor-only
cp ~/go/src/bitbucket.org/boguswojcik/mood-schema/mood.proto ./vendor/bitbucket.org/boguswojcik/mood-schema/
go generate ./
if go build main.go ; then
    printf "SUCCESS: Integration test for happy-consumer succeeded.\n\n"
else
    printf "FAILURE: Integration test for happy-consumer failed.\n\n"
fi

printf "TESTING: Testing sad-consumer...\n"
cd ~/schema-temp/src/bitbucket.org/boguswojcik
git clone -q git@bitbucket.org:boguswojcik/sad-consumer.git
cd ./sad-consumer
dep ensure --vendor-only
cp ~/go/src/bitbucket.org/boguswojcik/mood-schema/mood.proto ./vendor/bitbucket.org/boguswojcik/mood-schema/
go generate ./
if go build main.go ; then
    printf "SUCCESS: Integration test for sad-consumer succeeded.\n\n"
else
    printf "FAILURE: Integration test for sad-consumer failed.\n\n"
fi